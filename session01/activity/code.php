<?php
	

	// get full address

function getFullAddress($country, $city, $province, $specificAddress) {
    return "$specificAddress, $city, $province, $country";
}

function getLetterGrade($letterGrade){
	if($letterGrade >= 98){
		return 'A+';
	}
	else if($letterGrade >=95 && $letterGrade <= 97){
		return 'A';
	}
	else if($letterGrade >= 92 && $letterGrade <= 94){
		return 'A-';
	}
	else if($letterGrade >= 89 && $letterGrade <= 91){
		return 'B+';
	}
	else if($letterGrade >= 86 && $letterGrade <= 88){
		return 'B';
	}
	else if($letterGrade >= 83 && $letterGrade <= 85){
		return 'B-';
	}
	else if($letterGrade >= 80 && $letterGrade <= 82){
		return 'C+';
	}
	else if($letterGrade >= 77 && $letterGrade <= 79){
		return 'C';
	}
	else if($letterGrade >= 75 && $letterGrade <= 76){
		return 'C-';
	}
	else{
		return 'F';
	}
}
	?>



