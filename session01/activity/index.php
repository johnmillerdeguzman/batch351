<!-- This is used to includes another php file. -->
<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Acitivity - PHP Basics and Selection Control Structure</title>
	</head>
	<body>
		<h1>Full Address</h1>
		<p><?php echo getFullAddress("Philippines", "Metro Manila", "Quezon City", "3F Caswynn Bldg., Timog Avenuew"); ?></p>

		<h2>Letter-Based Grading</h2>
		<p><?php echo getLetterGrade(99); ?></p>
		<p><?php echo getLetterGrade(94); ?></p>
		<p><?php echo getLetterGrade(92); ?></p>
		<p><?php echo getLetterGrade(88); ?></p>
		<p><?php echo getLetterGrade(86); ?></p>
		<p><?php echo getLetterGrade(84); ?></p>
		<p><?php echo getLetterGrade(81); ?></p>
		<p><?php echo getLetterGrade(76); ?></p>
		<p><?php echo getLetterGrade(75); ?></p>
		<p><?php echo getLetterGrade(74); ?></p>
		<p><?php echo getLetterGrade(75); ?></p>


		
		</body>
</html>